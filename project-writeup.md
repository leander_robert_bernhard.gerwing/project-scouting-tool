# Projektbeschreibung

## **Basic info**

- Project title
- Leander Gerwing, 11145833, Medieninformatik Master
- https://git-ce.rwth-aachen.de/leander_robert_bernhard.gerwing/project-scouting-tool

## **Overview**

Statistikportale im Sportbereich wie FBRef erlauben es sowohl statistikbegeisterten Fans als auch Profiscouts, detaillierte und tiefgehende Statistiken über Mannschaften und Spieler einzusehen. Das Portal FBRef verwendet hierzu jedoch ausschließlich Tabellen, deren Lesbarkeit für einen durchschnittlichen Nutzer schwierig ist.
Das Ziel meiner App soll die einfache Lesbarkeit und Vergleichbarkeit von Statistiken von Profisportlern im Fußballbereich, die auch von Profiscouts verwendet werden.

## **Description of the data**

Das Datenset, welches visualisiert werden soll, enthält Daten zu allen Spielern der Saison 2023/24 aus verschiedenen Fußballligen der Herren. Diese betrachteten Ligen sind:

- England - Premier League
- Spanien - La Liga
- Deutschland - Bundesliga
- Italien - Serie A
- Frankreich - Ligue 1
- Brasilien - Série A
- Portugal - Primeira Liga
- Niederlande - Eredivisie
- Argentinien - Primera División
- Türkei - Süper Lig
- Belgien - Pro League
- England - EFL Championship
- Deutschland - 2. Bundesliga
- Österreich - Bundesliga

(Insgesamt 8.405) [Stand: 07.07.2024]. Die Datensätze enthalten allgemeine Attribute über den Spieler (Name, Position [kategorisch], Nationalität [kategorisch], Geburtsjahr [numerisch], Mannschaft [kategorisch], Liga [kategorisch]), sowie Statistiken für den Spieler zur Saison. Dazu zählen im Allgemeinen geläufige Statistiken wie Tore, Vorlagen und Spielminuten als auch erweiterte Statistiken (sofern verfügbar) wie erwartete Tore und Assists (xG, xA), Progressive Dribblings und Pässe (PrgC, PrgP), Pässe und angekommene Pässe, Schuss- und Torerzeugende Aktionen (SCA, GCA), Tacklings und gewonnene Tacklings und Ballberührungen, sowie für Torhüter Gegentore, gehaltene Schüsse in % und erwartete Gegentore nach Schüssen (PSxG) [alles numerische Daten]. Diese Statistiken werden zudem pro gespielten 90 Minuten des Spielers zur Verfügung gestellt.

Als abgeleitete Werte werden die erwarteten Werte wie xG oder xA in Differenz mit den tatsächlich erreichten Werten errechnet.

### **Preprocessing**

Die Datenquelle für die Statistiken ist das Statistikportal FBRef.com. Diese Daten werden mithilfe der Bibliothek SoccerData mit Python gescraped und verarbeitet. Dabei müssen mehrere Statistiken zu einem Pandas-DataFrame zusammengesetzt und anschließend als .csv ausgegeben werden. Die Berechnung der Differenzen zwischen erwarteten und tatsächlichen Werten wird on-the-fly in JavScript stattfinden.

## **Usage scenario & tasks**

Christoph ist Teil des Scouting-Teams eines großen Fußball-Clubs. Ein wichtiger Spieler hat den Verein vor kurzem verlassen und es wird nach einem passenden Ersatz gesucht. Um einen passenden Ersatz zu finden, möchte Christoph einen Datensatz erkunden, um einen Spieler zu finden, der durch seine Attribute und Statistiken den fehlenden Spieler möglichst passend ersetzen könnte. Zunächst sieht Christoph eine Übersicht aller Spieler, diese filtert er nun runter auf Spieler, die auf derselben Position spielen wie der verlorene Spieler. Um herauszufinden, welcher Spieler auch von der Spielweise her in das Team passen könnte, sucht Christoph den alten Spieler raus und schaut sich dessen Stärken an. Anschließend sortiert er den Datensatz nach Spielern, die ebenfalls ähnliche Stärken aufweisen wie der alte Spieler.

## **Description of visualization & initial sketch**

Die Spieler werden als einzelne Punkte aufgezeigt, die sich alle als Cluster um einen zentralen Punkt orientieren, wie bei einem Bubble-Chart. Für den Nutzer gibt es nun die Möglichkeit, diese Punkte nach einem der ordinalen Attribute zu gruppieren. Bei einer Neugruppierung werden die Punkte nach diesem Attribut geclustert.
Die Visualisierung muss durch die Menge an Daten zoombar und verschiebbar sein.
Zudem lassen sich Attribute auswählen, nach denen die Größe und die Farbe der Punkte bestimmen lässt. So lässt sich z.B. das Attribut “Progressive Pässe” für die Bestimmung der Größe der Punkte auswählen und das Attribut “Vorlagen” für die Einfärbung, wodurch diese sich in Verhältnis zueinander stellen lassen würden.
Die Punkte zeigen beim Hovern mit der Maus einen Tooltip mit Details zum Spieler an.

Es besteht die Möglichkeit, einen oder mehrere Spieler durch Klick auszuwählen. Mit einem Knopf lässt sich nun eine neue Visualisierung öffnen, die die ausgewählten Spieler in einem Radar-Chart vergleicht. Die Attribute des Radar Charts sind hierbei vom Nutzer durch Auswahlboxen veränderbar.
Bei Hover über einen Spieler wird dieser in der Visualisierung hervorgehoben.

Es soll eine Suche geben, mit dem Spieler anhand ihres Namens gefunden werden können.

![Slide 16_9 - 1.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/1c121570-6f48-48f1-9527-4e3deebc75b0/d67393aa-475c-4db5-9e4b-7b98375e3ced/Slide_16_9_-_1.png)

![Slide 16_9 - 1(1).png](https://prod-files-secure.s3.us-west-2.amazonaws.com/1c121570-6f48-48f1-9527-4e3deebc75b0/bec545ba-c0ee-4dc7-aebe-e33c9ee65334/Slide_16_9_-_1(1).png)

![Slide 16_9 - 1(2).png](https://prod-files-secure.s3.us-west-2.amazonaws.com/1c121570-6f48-48f1-9527-4e3deebc75b0/69f74fa5-2e66-422e-a1fb-a9c99531215b/Slide_16_9_-_1(2).png)

![Slide 16_9 - 4.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/1c121570-6f48-48f1-9527-4e3deebc75b0/b8dfa79c-115e-4d63-acb2-d419e817384d/Slide_16_9_-_4.png)

## **Work breakdown and schedule**

### Meilenstein 2

- Preprocessing vervollständigt (8h) [bis 14.06.]
- Funktionierendes Bubble-Chart mit Suche und Tooltip (20h) [bis 07.07.]

### Meilenstein 3

- Einbau von “Gruppierung”, “Einfärbung” und “Größen” Einstellungen (24-32h) [bis 28.07.]
- Einbau Spielerauswahl und Vergleich mit Radar Charts (24-32h) [bis 14.09.]
