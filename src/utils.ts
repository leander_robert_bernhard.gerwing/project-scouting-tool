export function debounce(func: Function, timeout = 300){
    let timer: number | undefined;
    return (...args: any[]) => {
        clearTimeout(timer);
        timer = setTimeout(() => { // @ts-ignore
            func.apply(this, args); }, timeout);
    };
}