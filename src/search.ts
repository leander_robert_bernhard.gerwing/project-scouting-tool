import SearchableChart from "@/charts/SearchableChart.ts";

export default class Search {
    dataList: HTMLSelectElement
    constructor(chart: SearchableChart) {
        this.dataList = document.querySelector('#player-datalist') as HTMLSelectElement
        const searchInput = document.querySelector('#search') as HTMLInputElement
        searchInput.onkeydown = (e) =>
            e.key === 'Enter' ? chart.search(searchInput.value) : null

        const searchButton = document.querySelector('#search-button') as HTMLButtonElement
        searchButton.onclick = () => chart.search(searchInput.value)
    }

    initOptions (data: any) {
        for (const player of data) {
            const option = document.createElement('option')
            option.value = player['player']
            this.dataList.appendChild(option)
        }
    }
}