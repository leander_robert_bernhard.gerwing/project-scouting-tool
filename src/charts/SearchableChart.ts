import Chart from "@/charts/chart.ts";

export default abstract class SearchableChart extends Chart {
    constructor(data: any, _config: any) {
        super(data, _config);
    }

    abstract search(input: string): void
}