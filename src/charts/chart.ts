export type ChartConfig = {
    parentElement: HTMLElement,
    containerWidth: number,
    containerHeight: number,
    margin: {
        top: number,
        bottom: number,
        right: number,
        left: number,
    },
    tooltipPadding: number,
}

export type ChartConfigParam = Partial<ChartConfig> & {
    parentElement: string | HTMLElement,
}

export default abstract class Chart {
    config: ChartConfig
    data: any

    constructor(data: any, _config: ChartConfigParam) {
        this.data = data
        this.config = {
            ..._config,
            parentElement: typeof _config.parentElement === 'string' ? document.querySelector(_config.parentElement) as HTMLElement : _config.parentElement,
            containerWidth: _config.containerWidth || 500,
            containerHeight: _config.containerHeight || 140,
            margin: _config.margin || { top: 10, bottom: 30, right: 10, left: 30 },
            tooltipPadding: _config.tooltipPadding || 15
        }
    }

    abstract initVis(): void
    abstract updateVis(data: any): void
    abstract renderVis(): void

    height () {
        return this.config.containerHeight - this.config.margin.top - this.config.margin.bottom
    }

    width () {
        return this.config.containerWidth - this.config.margin.left - this.config.margin.right
    }
}