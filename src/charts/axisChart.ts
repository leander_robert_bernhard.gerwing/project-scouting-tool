import * as d3 from "d3";
import Chart, {ChartConfigParam} from "@/charts/chart.ts";
import {ScaleBand, ScaleContinuousNumeric, ScaleDiverging, ScaleLinear, ScaleLogarithmic, ScaleTime} from "d3";
import {AxisScale} from "d3-axis";

export type AxisChartConfigParam<XType, YType> = ChartConfigParam & {
    xAccessor: (d: any) => XType,
    yAccessor: (d: any) => YType,
    vertical: boolean
}

// Scale type is a union of all the d3 scale types
export type Scale = ScaleLinear<any, any> |
    ScaleTime<any, any> |
    ScaleBand<any> |
    ScaleDiverging<any> |
    ScaleContinuousNumeric<any, any> |
    ScaleLogarithmic<any, any>

export default abstract class AxisChart<XType, YType> extends Chart {
    xScale: AxisScale<any>;
    yScale: AxisScale<any>;
    xAxis: d3.Axis<any>;
    yAxis: d3.Axis<any>;
    xAxisGroup: d3.Selection<any, any, any, any> | undefined;
    yAxisGroup: d3.Selection<any, any, any, any> | undefined;
    xAccessor: (d: any) => XType;
    yAccessor: (d: any) => YType;
    abstract chartId: string;
    chart: any
    vertical: boolean

    constructor(data: any[], _config: AxisChartConfigParam<XType, YType>) {
        super(data, _config)

        this.xAccessor = _config.xAccessor
        this.yAccessor = _config.yAccessor
        const xScale = this.getXScale()
        const yScale = this.getYScale()
        if (!xScale || !yScale) {
            throw new Error('Scales could not be created. Check the data types.')
        }
        this.xScale = xScale
            .range([0, this.width()]) as AxisScale<any>
        this.xAxis = d3.axisBottom(this.xScale).tickSizeOuter(0);

        this.yScale = yScale
            .range([0, this.height()]) as AxisScale<any>

        this.yAxis = d3.axisLeft(this.yScale).tickSizeOuter(0)

        this.vertical = _config.vertical ?? false
    }

    initVis() {
        let vis = this;
        vis.config.parentElement.innerHTML += `
            <svg id="${this.chartId}"></svg>
        `;
        vis.config.parentElement.innerHTML += `
            <div id="tooltip"></div>
        `;

        const svg = d3.select(`#${this.chartId}`)
            .attr('width', vis.config.containerWidth)
            .attr('height', vis.config.containerHeight)

        vis.chart = svg.append('g')
            .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`)

        vis.xAxisGroup = vis.chart.append('g')
            .attr('class', 'axis x-axis')
            .call(vis.xAxis)
            .attr("transform", `translate(0, ${vis.config.containerHeight - vis.config.margin.bottom - vis.config.margin.top})`)

        if (!this.vertical) {
            (vis.yScale as Scale).domain(vis.yScale.domain().reverse())
        }

        vis.yAxisGroup = vis.chart.append('g')
            .attr('class', 'axis y-axis')
            .call(vis.yAxis)
    }

    getScale(accessor: (d: any) => XType | YType): Scale | undefined {
        const dataPoint = accessor(this.data[0])
        if (typeof dataPoint === 'number') {
            return d3.scaleLinear()
                .domain([0, this.max(accessor as (d: any) => number)])
        } else if (dataPoint instanceof Date) {
            return d3.scaleTime()
                .domain([
                    this.min(accessor as (d: any) => Date),
                    this.max(accessor as (d: any) => Date)
                ])
        } else if (typeof dataPoint === 'string') {
            return d3.scaleBand()
                .domain(this.data.map((d: any) => accessor(d)))
                .paddingInner(0.05)
        }
        return undefined
    }

    getXScale(): Scale | undefined {
        return this.getScale(this.xAccessor)
    }

    getYScale(): Scale | undefined {
        return this.getScale(this.yAccessor)
    }

    min(accessor: (d: any) => number | Date) {
        return d3.min(this.data, accessor) ?? 0
    }

    max(accessor: (d: any) => number | Date) {
        return d3.max(this.data, accessor) ?? 0
    }
}