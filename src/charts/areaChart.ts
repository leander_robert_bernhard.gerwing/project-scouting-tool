import * as d3 from "d3";
import {Axis, ScaleLinear, ScaleTime} from "d3";
import AxisChart, {AxisChartConfigParam} from "@/charts/axisChart.ts";


export default class AreaChart extends AxisChart<Date, number> {
    chartId = 'areaChart';
    xScale: ScaleTime<number, number, never>;
    yScale: ScaleLinear<number, number, never>;
    xAxis: Axis<any>;
    yAxis: Axis<any>;
    minY: number = 0
    maxY: number = 0

    constructor(data: any[], config: AxisChartConfigParam<Date, number>) {
        super(data, config)

        this.xAccessor = config.xAccessor
        this.yAccessor = config.yAccessor

        this.xScale = d3.scaleTime()
        this.yScale = d3.scaleLinear()
        this.xAxis = d3.axisBottom(this.xScale)
        this.yAxis = d3.axisLeft(this.yScale)

        this.initVis()
    }

    initVis(): void {
        super.initVis()
        let vis = this;

        vis.xScale = vis.xScale
            .domain([
                vis.min(vis.xAccessor),
                vis.max(vis.xAccessor),
            ])
            .range([0, vis.config.containerWidth - vis.config.margin.left - vis.config.margin.right]);

        vis.yScale = vis.yScale
            .domain([
                vis.min(vis.yAccessor),
                vis.max(vis.yAccessor),
            ])
            .nice()
            .range([0, vis.config.containerHeight - vis.config.margin.top - vis.config.margin.bottom])
        vis.maxY = vis.yScale.domain()[0]
        vis.minY = vis.yScale.domain()[1]

        vis.xAxis = d3.axisBottom(vis.xScale).tickSizeOuter(0)

        vis.yAxis = d3.axisLeft(vis.yScale).tickSizeOuter(0);

        vis.chart.append('g')
            .attr('class', 'axis x-axis')
            .call(vis.xAxis)
            .attr("transform", `translate(0, ${vis.config.containerHeight - vis.config.margin.bottom - vis.config.margin.top})`)

        vis.chart.append('g')
            .attr('class', 'axis y-axis')
            .call(vis.yAxis)
            .call(vis.yAxis)
    }

    updateVis(data: any[]): void {
        this.data = data
    }

    renderVis(): void {
        let vis: AreaChart = this;

        if (!vis.xScale || !vis.yScale || !vis.xAxis || !vis.yAxis) return

        const xScale: NonNullable<typeof vis.xScale> = vis.xScale
        const yScale: NonNullable<typeof vis.yScale> = vis.yScale

        const chart = vis.chart

        const parsedData = vis.data.map((d: any) => [xScale(vis.xAccessor(d)), yScale(vis.yAccessor(d))])

        const line = d3.line()
        const area = d3.area()
            .y0(vis.yScale(vis.minY))

        chart.append('path')
            .attr('d', area(parsedData))
            .attr('fill', 'rgba(62,187,228,0.3)')
            .attr('stroke', 'none')
            .attr('stroke-width', 2)
        chart.append('path')
            .attr('d', line(parsedData))
            .attr('fill', 'none')
            .attr('stroke', 'steelblue')
            .attr('stroke-width', 2)
    }
}