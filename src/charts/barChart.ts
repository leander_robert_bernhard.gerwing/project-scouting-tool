import * as d3 from "d3";
import AxisChart, {AxisChartConfigParam, Scale} from "@/charts/axisChart.ts";
import {ScaleBand} from "d3-scale";
import {AxisScale} from "d3-axis";

type BarChartConfigParam<XType, YType> = AxisChartConfigParam<XType, YType>

export default class BarChart<XType, YType> extends AxisChart<XType, YType> {
    chartId = 'barChart'

    constructor(data: any[], config: BarChartConfigParam<XType, YType>) {
        super(data, config)

        this.initVis()
    }

    initVis(): void {
        super.initVis()
    }

    updateVis(data: any[]): void {
        this.data = data;

        (this.xScale as Scale).domain((this.getXScale() as AxisScale<any>).domain())

        this.yAxisGroup?.transition()
            .duration(1000)
            .call(this.yAxis);
        this.xAxisGroup?.transition()
            .duration(1000)
            .call(this.xAxis);

        d3.selectAll('rect')
            .transition()
            .duration(1000)
            .attr('x', (d: any) => this.xScale(this.xAccessor(d)) ?? 0);
    }

    renderVis(): void {
        let vis = this;

        if (!vis.xScale || !vis.yScale || !vis.xAxis || !vis.yAxis) return

        const chart = vis.chart

        const bars = chart.selectAll('rect')
            .data(vis.data)
            .enter()
            .append('rect')
            .attr('fill', 'steelblue')
            .attr('pointer-events', 'all')
        if (vis.vertical) {
            bars.attr('y', (d: any) => (vis.yScale(vis.yAccessor(d))))
                .attr('width', (d: any) => (vis.xScale(vis.xAccessor(d))))
                .attr('height', (vis.yScale as ScaleBand<any>).bandwidth() ?? 0)
        } else {
            bars.attr('x', (d: any) => vis.xScale(vis.xAccessor(d)))
                .attr('y', (d: any) => vis.yScale(vis.yAccessor(d)))
                .attr('height', (d: any) => (vis.height() - (vis.yScale(vis.yAccessor(d)) ?? 0)))
                .attr('width', (vis.xScale as ScaleBand<any>).bandwidth() ?? 0)
        }

        bars.on('mouseover', (_: Event, d: any) => {
            d3.select('#tooltip')
                .style('display', 'block')
                // Format number with million and thousand separator
                .html(`<div class="tooltip-label">Population</div>${d3.format(',')(d.population)}`);
        })
            .on('mousemove', (event: any) => {
                d3.select('#tooltip')
                    .style('left', (event.pageX + vis.config.tooltipPadding) + 'px')
                    .style('top', (event.pageY + vis.config.tooltipPadding) + 'px')
            })
            .on('mouseleave', () => {
                d3.select('#tooltip').style('display', 'none');
            });
    }
}