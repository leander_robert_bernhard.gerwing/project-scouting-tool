import * as d3 from "d3";
import {HierarchyNode, NumberValue, ScaleLinear, ScaleOrdinal, ScaleSequential} from "d3";
import {ChartConfig, ChartConfigParam} from "@/charts/chart.ts";
import SearchableChart from "@/charts/SearchableChart.ts";
import {debounce} from "@/utils.ts";
// @ts-ignore
import Legend from "@/charts/utils/legendColor.js";
// @ts-ignore
import legendCircle from "@/charts/utils/legendCircle.js";

export type BubbleChartConfig = ChartConfig & {
    groupAccessor: (d: any) => string | null,
    sizeAccessor: (d: any) => number,
    colorAccessor: (d: any) => string | number | null,
    idAccessor: (d: any) => any,
    zoomExtent: [number, number],
    onZoom?: (event: any) => void,
    renderTooltip?: (dataPoint: any, tooltip: d3.Selection<d3.BaseType, unknown, HTMLElement, any>) => void,
    onClick?: (dataPoint: any) => void,
}

export type BubbleChartConfigParam = ChartConfigParam & Partial<BubbleChartConfig>

export default class BubbleChart extends SearchableChart {
    chartId: string = 'bubbleChart';
    chart: any
    zoom: any;
    config: BubbleChartConfig
    packRoot: HierarchyNode<any> | null = null
    highlightedNode: HierarchyNode<any> | null = null
    colorScale: ScaleSequential<any> | ScaleOrdinal<any, any> | null = null
    sizeScale: ScaleLinear<any, number> | null = null
    groupLabels: any | null = null
    groupsWithLabels: any[] = []
    defaultSizeAccessor = (_: any) => 5

    constructor(data: any[], _config: BubbleChartConfigParam) {
        super(data, _config as ChartConfigParam)

        this.config = this.createConfig(_config)

        this.initVis()
    }

    private setConfig(_config: BubbleChartConfigParam) {
        this.config = this.createConfig(_config)
    }

    private createConfig(_config: BubbleChartConfigParam) {
        return {
            ..._config,
            parentElement: typeof _config.parentElement === 'string' ? document.querySelector(_config.parentElement) as HTMLElement : _config.parentElement,
            containerWidth: _config.containerWidth || 500,
            containerHeight: _config.containerHeight || 140,
            margin: _config.margin || {top: 10, bottom: 30, right: 10, left: 30},
            tooltipPadding: _config.tooltipPadding || 30,
            groupAccessor: _config.groupAccessor || (() => null),
            sizeAccessor: _config.sizeAccessor || this.defaultSizeAccessor,
            colorAccessor: _config.colorAccessor || (() => null),
            idAccessor: _config.idAccessor || (() => null),
            zoomExtent: _config.zoomExtent || [0.5, 20],
            renderTooltip: _config.renderTooltip || (() => null),
            onClick: _config.onClick || (() => null),
        }
    }

    initVis() {
        let vis = this;
        vis.config.parentElement.innerHTML += `
            <svg id="${this.chartId}"></svg>
        `;
        vis.config.parentElement.innerHTML += `
            <div id="tooltip" class="tooltip"></div>
        `;
        vis.config.parentElement.innerHTML += `
            <div id="color-legend"></div>
        `;

        const svg = d3.select(`#${this.chartId}`)
            .attr('width', vis.config.containerWidth)
            .attr('height', vis.config.containerHeight)
            .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`)

        vis.chart = svg.append('g')

        vis.zoom = d3.zoom()
            .scaleExtent(vis.config.zoomExtent)
            .on('zoom', (event) => {
                vis.chart.attr('transform', event.transform);
                vis.config.onZoom?.(event)
            });

        vis.chart.call(vis.zoom)

        vis.updateColorScale()
        vis.updateSizeScale()
    }

    updateVis(data: any[]): void {
        this.data = data;
        this.update()
    }

    updateVisConfig(_config: BubbleChartConfigParam): void {
        this.setConfig({...this.config, ..._config})
        this.update()
    }

    private update() {
        let vis: BubbleChart = this;

        vis.updateSizeScale()
        vis.updatePackRoot()
        vis.updateColorScale()

        const node = vis.chart
            .selectAll("g")
            .filter('[bubble]')
            .data(vis.packRoot?.leaves(), (d: any) => vis.config.idAccessor(d.data))
            .join(
                (enter: any) => enter.append("g"),
                (update: any) => update
                    .transition()
                    .duration(1000)
                    .attr("transform", (d: any) => `translate(${d.x},${d.y})`),
                (exit: any) => exit.remove()
            )
        node.selectAll('text').remove()

        node.selectAll('circle')
            .data((d: any) => [d])
            .transition()
            .duration(1000)
            .attr('fill', (d: any) => vis.getFillForNode(d))
            .attr('r', (d: any) => d.r)

        /*setTimeout(() => {
            node
                .filter((d: any) => d.r > 25)
                .append('text')
                .text((d: any) => vis.config.sizeAccessor(d.data))
                .attr('text-anchor', 'middle')
                .attr('opacity', '0')
                .transition()
                .duration(100)
                .attr('opacity', '1')
        }, 1000)*/
    }

    renderVis(): void {
        let vis: BubbleChart = this;

        vis.updatePackRoot()

        const node = vis.chart
            .selectAll("g")
            .data(vis.packRoot?.leaves(), (d: any) => vis.config.idAccessor(d.data))
            .join("g")
            .attr("bubble", true)
            .attr("transform", (d: any) => `translate(${d.x},${d.y})`)

        node
            .append('circle')
            .attr('fill', (d: any) => vis.getFillForNode(d))
            .attr('r', (d: any) => d.r)
            .attr('data-bubble', (d: any) => vis.config.idAccessor(d.data))
            .on('mouseover', (_: Event, d: any) => {
                const element = d3.select('#tooltip')
                    .style('display', 'block')
                if (!vis.config.renderTooltip) return
                vis.config.renderTooltip(d.data, element)
            })
            .on('mousemove', (event: any) => {
                d3.select('#tooltip')
                    .style('left', (event.layerX + vis.config.tooltipPadding) + 'px')
                    .style('top', (event.layerY + vis.config.tooltipPadding) + 'px')
            })
            .on('mouseleave', (_: Event) => {
                d3.select('#tooltip').style('display', 'none');
            })
            .on('click', (_: any, d: any) => {
                vis.config.onClick?.(d.data)
            })
    }

    updateSelectedNodes(nodes: any[]) {
        d3.selectAll('circle[data-bubble]')
            .attr('stroke', 'none')
        for (const node of nodes) {
            const playerNode = this.getNode(node)
            playerNode
                .attr('stroke', node._color || "#333")
                .attr('stroke-width', 2)
        }
    }

    private renderGroupLabels() {
        this.groupLabels = this.chart.selectAll('g')
            .filter('[group]')
            .data(this.groupsWithLabels)
            .join(
                (enter: any) => {
                    const enterGroup = enter.append("g")
                        .attr('opacity', 0)
                    enterGroup
                        .transition()
                        .delay(1000)
                        .duration(1000)
                        .attr('opacity', 1)
                    return enterGroup
                },
                (update: any) => {
                    update.selectAll('*').remove()
                    return update
                },
                (exit: any) => exit.transition(300)
                    .attr('opacity', 0)
                    .remove()
            )
            .attr('group', true)
            .attr("transform", (d: any) => `translate(${d.x},${d.y})`)
            .on('mouseover', (event: Event) => {
                const target = event.target as HTMLElement
                // hide label
                d3.select(target.closest('g'))
                    .selectAll("*")
                    .transition()
                    .duration(100)
                    .attr('opacity', 0)
            })
            .on('mouseleave', (event: Event) => {
                const target = event.target as HTMLElement
                // show label
                d3.select(target.closest('g'))
                    .selectAll("*")
                    .transition()
                    .duration(100)
                    .attr('opacity', 1)
                    .attr('width', (d: any) => d.bbox.width + 10)
            })

        this.groupLabels.transition()
            .delay(1000)
            .duration(1000)
            .attr('opacity', 1)

        const rects = this.groupLabels
            .selectAll('rect')
            .filter((d: any) => d.data[0])
            .data((d: any) => [d])
            .join(
                (enter: any) => enter.append("rect"),
                (update: any) => update,
                (exit: any) => exit.remove()
            )
            .attr('fill', 'white')
            .attr('stroke', '#333')
            .attr('stroke-width', 1 / this.zoomLevel)
            .attr('rx', 5 / this.zoomLevel)
            .attr('ry', 5 / this.zoomLevel)

        this.groupLabels
            .selectAll('text')
            .data((d: any) => [d])
            .join(
                (enter: any) => enter.append("text"),
                (update: any) => update,
                (exit: any) => exit.remove()
            )
            .attr('text-anchor', 'middle')
            .attr('alignment-baseline', 'middle')
            .attr('font-size', 12 / this.zoomLevel)
            .attr('fill', 'black')
            .text((d: any) => d.data[0])
            .call((selection: any) => selection.each(
                function (d: any) {
                    // I can't get TS to type annotate 'this' correctly here
                    // @ts-ignore
                    d.bbox = this.getBBox()
                }
            ))
        rects.attr('x', (d: any) => d.bbox.x - 5)
            .attr('y', (d: any) => d.bbox.y - 5)
            .attr('width', (d: any) => d.bbox.width + 10)
            .attr('height', (d: any) => d.bbox.height + 10)
    }

    private updateGroupsWithLabels = debounce(() => {
        if (!this.packRoot) {
            this.groupsWithLabels = []
            return
        }
        const groups = this.packRoot.descendants()
            .filter(group =>
                group.children && group.value &&
                group.value >= (50 / this.zoomLevel) &&
                group.value < (300 / (this.zoomLevel * 2.5)) &&
                group.children.length > 5
            )

        this.groupsWithLabels = groups.length > 1 ? groups : []
        this.renderGroupLabels()
    }, 300)


    /**
     * Updates the d3 hierachy pack root with the current data.
     * Uses the groupAccessor and sizeAccessor to group the data.
     * @private
     */
    private updatePackRoot() {
        const groupedData = d3.group(
            this.data,
            (d: any) => this.config.groupAccessor(d)
        )
        const hierarchy = (d3.hierarchy(groupedData) as HierarchyNode<any>)
            .sum((d: any) => this.getSizeForNode(d))
            .sort((a: any, b: any) => d3.descending(a.value, b.value))
        this.packRoot = d3.pack()
            .size([this.width(), this.height()])
            .padding(2)
            (hierarchy)
        this.updateGroupsWithLabels()
    }

    private getFillForNode(node: HierarchyNode<any>) {
        if (node.children) return "transparent"
        if (!this.colorScale) return 'var(--primary)'
        return this.colorScale(this.config.colorAccessor(node.data) as NumberValue)
    }

    private getSizeForNode(dataPoint: HierarchyNode<any>): number {
        if (!dataPoint || !this.config.sizeAccessor(dataPoint)) return 0.1
        if (!this.sizeScale) return 5
        return this.sizeScale(this.config.sizeAccessor(dataPoint) as NumberValue)
    }

    search(input: string): void {
        if (!input) return
        if (!input || !this.packRoot) return
        const result = this.packRoot.leaves()
            .find((d: any) => this.config.idAccessor(d.data).toLowerCase().includes(input.toLowerCase()))
        if (result) {
            this.zoomToPoint(result)
        }
    }

    updateColorScale() {
        // The color scale is either a sequential scale or an ordinal scale
        // depending on what type the colorAccessor returns
        const sampleValue = this.config.colorAccessor(this.data[0])
        if (!sampleValue) {
            this.colorScale = null
        } else if (typeof sampleValue === 'number') {
            this.colorScale = d3.scaleSequential(d3.interpolateCool)
                .domain(
                    d3.extent(
                        this.data,
                        this.config.colorAccessor as (d: any) => number | null
                    ) as [number, number]
                )

            const colorLegend = document.getElementById('color-legend')
            if (colorLegend) {
                colorLegend.replaceChildren(Legend(this.colorScale, d3.interpolateCool))
            }
        } else {
            this.colorScale = d3.scaleOrdinal(
                new Set(this.data.map((d: any) => this.config.colorAccessor(d))),
                d3.schemeCategory10,
            )
        }

    }

    updateSizeScale() {
        this.sizeScale = d3.scaleLinear(
            d3.extent(
                this.data,
                this.config.sizeAccessor as (d: any) => number | null
            ) as [number, number],
            [0, 10]
        )
    }

    get zoomLevel() {
        return d3.zoomTransform(this.chart.node()).k
    }

    zoomIn() {
        this.chart.transition()
            .duration(300)
            .call(this.zoom.scaleBy, 1.3)
        this.updateGroupsWithLabels()
    }

    zoomOut() {
        this.chart.transition()
            .duration(300)
            .call(this.zoom.scaleBy, 0.75)
        this.updateGroupsWithLabels()
    }

    zoomTo(zoomLevel: number) {
        this.chart.call(this.zoom.scaleTo, zoomLevel)
        this.updateGroupsWithLabels()
    }

    zoomToPoint(dataPoint: any) {
        if (dataPoint) {
            if (typeof dataPoint === 'string') {
                dataPoint = this.packRoot?.leaves().find((d: any) => this.config.idAccessor(d.data) === dataPoint)
            }
            if (this.highlightedNode) {
                this.getNode(this.highlightedNode.data)
                    .transition()
                    .duration(700)
                    .attr('fill', (d: any) => this.getFillForNode(d))
            }
            this.getNode(dataPoint.data)
                .transition()
                .duration(700)
                .attr('fill', 'var(--secondary)')
            this.highlightedNode = dataPoint
            this.chart.transition()
                .duration(300)
                .call(
                    this.zoom.transform,
                    d3.zoomIdentity
                        .scale(Math.max(this.config.zoomExtent[1], this.config.zoomExtent[0] / dataPoint.r))
                        .translate(
                            -dataPoint.x + (this.width() / (2 * this.config.zoomExtent[1])),
                            -dataPoint.y + (this.height() / (2 * this.config.zoomExtent[1]))
                        )
                )
        }
        this.updateGroupsWithLabels()
    }

    private getNode(data: any) {
        return d3.select(`circle[data-bubble="${this.config.idAccessor(data)}"]`);
    }
}