export const positions = [
    {
        "pos": "FW",
        "name": "Stürmer"
    },
    {
        "pos": "MF",
        "name": "Mittelfeldspieler"
    },
    {
        "pos": "DF",
        "name": "Verteidiger"
    },
    {
        "pos": "GK",
        "name": "Torwart"
    }
]

export const getPositionName = (pos: string) => {
    const splitPositions = pos.split(',')
    return splitPositions
        .map(p => positions.find(c => c.pos === p)?.name ?? p)
        .join(', ')
}