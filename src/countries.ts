// Maps the three-letter country code to the german country name

export const countries = [
    {
        "code": "GER",
        "name": "Deutschland",
    },
    {
        "code": "FRA",
        "name": "Frankreich",
    },
    {
        "code": "ITA",
        "name": "Italien",
    },
    {
        "code": "ENG",
        "name": "England",
    },
    {
        "code": "ESP",
        "name": "Spanien",
    },
    {
        "code": "USA",
        "name": "Vereinigte Staaten",
    },
    {
        "code": "CAN",
        "name": "Kanada",
    },
    {
        "code": "AUS",
        "name": "Australien",
    },
    {
        "code": "BRA",
        "name": "Brasilien",
    },
    {
        "code": "CHN",
        "name": "China",
    },
    {
        "code": "JPN",
        "name": "Japan",
    },
    {
        "code": "RUS",
        "name": "Russland",
    },
    {
        "code": "IND",
        "name": "Indien",
    },
    {
        "code": "MEX",
        "name": "Mexiko",
    },
    {
        "code": "ARG",
        "name": "Argentinien",
    },
    {
        "code": "RSA",
        "name": "Südafrika",
    },
    {
        "code": "KOR",
        "name": "Südkorea",
    },
    {
        "code": "TUR",
        "name": "Türkei",
    },
    {
        "code": "EGY",
        "name": "Ägypten",
    },
    {
        "code": "SWE",
        "name": "Schweden",
    },
    {
        "code": "NOR",
        "name": "Norwegen",
    },
    {
        "code": "DEN",
        "name": "Dänemark",
    },
    {
        "code": "FIN",
        "name": "Finnland",
    },
    {
        "code": "SUI",
        "name": "Schweiz",
    },
    {
        "code": "AUT",
        "name": "Österreich",
    },
    {
        "code": "BEL",
        "name": "Belgien",
    },
    {
        "code": "NED",
        "name": "Niederlande",
    },
    {
        "code": "POL",
        "name": "Polen",
    },
    {
        "code": "GRE",
        "name": "Griechenland",
    },
    {
        "code": "HUN",
        "name": "Ungarn",
    },
    {
        "code": "CZE",
        "name": "Tschechien",
    },
    {
        "code": "SVK",
        "name": "Slowakei",
    },
    {
        "code": "POR",
        "name": "Portugal",
    },
    {
        "code": "IRL",
        "name": "Irland",
    },
    {
        "code": "ISL",
        "name": "Island",
    },
    {
        "code": "NZL",
        "name": "Neuseeland",
    },
    {
        "code": "SGP",
        "name": "Singapur",
    },
    {
        "code": "THA",
        "name": "Thailand",
    },
    {
        "code": "IDN",
        "name": "Indonesien",
    },
    {
        "code": "MYS",
        "name": "Malaysia",
    },
    {
        "code": "PHL",
        "name": "Philippinen",
    },
    {
        "code": "VNM",
        "name": "Vietnam",
    },
    {
        "code": "SAU",
        "name": "Saudi-Arabien",
    },
    {
        "code": "ISR",
        "name": "Israel",
    },
    {
        "code": "IRN",
        "name": "Iran",
    },
    {
        "code": "PAK",
        "name": "Pakistan",
    },
    {
        "code": "BGD",
        "name": "Bangladesch",
    },
    {
        "code": "NGA",
        "name": "Nigeria",
    },
    {
        "code": "ETH",
        "name": "Äthiopien",
    },
    {
        "code": "KEN",
        "name": "Kenia",
    },
    {
        "code": "UGA",
        "name": "Uganda",
    },
    {
        "code": "TZA",
        "name": "Tansania",
    },
    {
        "code": "GHA",
        "name": "Ghana",
    },
    {
        "code": "MAR",
        "name": "Marokko",
    },
    {
        "code": "TUN",
        "name": "Tunesien",
    },
    {
        "code": "LBN",
        "name": "Libanon",
    },
    {
        "code": "JOR",
        "name": "Jordanien",
    },
    {
        "code": "IRQ",
        "name": "Irak",
    },
    {
        "code": "SYR",
        "name": "Syrien",
    },
    {
        "code": "KWT",
        "name": "Kuwait",
    },
    {
        "code": "QAT",
        "name": "Katar",
    },
    {
        "code": "ARE",
        "name": "Vereinigte Arabische Emirate",
    },
    {
        "code": "OMN",
        "name": "Oman",
    },
    {
        "code": "YEM",
        "name": "Jemen",
    },
    {
        "code": "AFG",
        "name": "Afghanistan",
    },
    {
        "code": "NPL",
        "name": "Nepal",
    },
    {
        "code": "BTN",
        "name": "Bhutan",
    },
    {
        "code": "LKA",
        "name": "Sri Lanka",
    },
    {
        "code": "MMR",
        "name": "Myanmar",
    },
    {
        "code": "KHM",
        "name": "Kambodscha",
    },
    {
        "code": "LAO",
        "name": "Laos",
    },
    {
        "code": "MNG",
        "name": "Mongolei",
    },
    {
        "code": "PRK",
        "name": "Nordkorea",
    },
    {
        "code": "KAZ",
        "name": "Kasachstan",
    },
    {
        "code": "UZB",
        "name": "Usbekistan",
    },
    {
        "code": "TKM",
        "name": "Turkmenistan",
    },
    {
        "code": "KGZ",
        "name": "Kirgisistan",
    },
    {
        "code": "TJK",
        "name": "Tadschikistan",
    },
    {
        "code": "ARM",
        "name": "Armenien",
    },
    {
        "code": "GEO",
        "name": "Georgien",
    },
    {
        "code": "AZE",
        "name": "Aserbaidschan",
    },
    {
        "code": "SRB",
        "name": "Serbien",
    },
    {
        "code": "CRO",
        "name": "Kroatien",
    },
    {
        "code": "SVN",
        "name": "Slowenien",
    },
    {
        "code": "BIH",
        "name": "Bosnien und Herzegowina",
    },
    {
        "code": "MKD",
        "name": "Nordmazedonien",
    },
    {
        "code": "ALB",
        "name": "Albanien",
    },
    {
        "code": "MNE",
        "name": "Montenegro",
    },
    {
        "code": "KVX",
        "name": "Kosovo",
    },
    {
        "code": "MLT",
        "name": "Malta",
    },
    {
        "code": "CYP",
        "name": "Zypern",
    },
    {
        "code": "EST",
        "name": "Estland",
    },
    {
        "code": "LVA",
        "name": "Lettland",
    },
    {
        "code": "LTU",
        "name": "Litauen",
    },
    {
        "code": "BLR",
        "name": "Weißrussland",
    },
    {
        "code": "UKR",
        "name": "Ukraine",
    },
    {
        "code": "MDA",
        "name": "Moldawien",
    },
    {
        "code": "GNB",
        "name": "Guinea-Bissau",
    },
    {
        "code": "CIV",
        "name": "Elfenbeinküste",
    },
    {
        "code": "GAB",
        "name": "Gabun",
    },
    {
        "code": "CMR",
        "name": "Kamerun",
    },
    {
        "code": "CGO",
        "name": "Kongo",
    },
    {
        "code": "ZIM",
        "name": "Simbabwe",
    },
    {
        "code": "ZAM",
        "name": "Sambia",
    },
    {
        "code": "MOZ",
        "name": "Mosambik",
    },
    {
        "code": "MWI",
        "name": "Malawi",
    },
    {
        "code": "ANG",
        "name": "Angola",
    },
    {
        "code": "NAM",
        "name": "Namibia",
    },
    {
        "code": "BWA",
        "name": "Botswana",
    },
    {
        "code": "SWZ",
        "name": "Swasiland",
    },
    {
        "code": "LSO",
        "name": "Lesotho",
    },
    {
        "code": "LBY",
        "name": "Libyen",
    },
    {
        "code": "SDN",
        "name": "Sudan",
    },
    {
        "code": "SSD",
        "name": "Südsudan",
    },
    {
        "code": "ERI",
        "name": "Eritrea",
    },
    {
        "code": "DJI",
        "name": "Dschibuti",
    },
    {
        "code": "SOM",
        "name": "Somalia",
    },
    {
        "code": "SEN",
        "name": "Senegal",
    },
    {
        "code": "ALG",
        "name": "Algerien",
    },
    {
        "code": "URU",
        "name": "Uruguay",
    },
    {
        "code": "COL",
        "name": "Kolumbien",
    },
    {
        "code": "PER",
        "name": "Peru",
    },
    {
        "code": "VEN",
        "name": "Venezuela",
    },
    {
        "code": "ECU",
        "name": "Ecuador",
    },
    {
        "code": "BOL",
        "name": "Bolivien",
    },
    {
        "code": "PAR",
        "name": "Paraguay",
    },
    {
        "code": "GUY",
        "name": "Guyana",
    },
    {
        "code": "SUR",
        "name": "Suriname",
    },
    {
        "code": "GUF",
        "name": "Französisch-Guayana",
    },
    {
        "code": "PAN",
        "name": "Panama",
    },
    {
        "code": "ROU",
        "name": "Rumänien",
    },
    {
        "code": "LUX",
        "name": "Luxemburg",
    },
    {
        "code": "JAM",
        "name": "Jamaika",
    },
    {
        "code": "SCO",
        "name": "Schottland",
    },
    {
        "code": "MLI",
        "name": "Mali",
    },
    {
        "code": "WAL",
        "name": "Wales",
    },
    {
        "code": "BUL",
        "name": "Bulgarien",
    },
    {
        "code": "CRC",
        "name": "Costa Rica",
    },
    {
        "code": "COD",
        "name": "Demokratische Republik Kongo",
    },
    {
        "code": "NIR",
        "name": "Nordirland",
    },
    {
        "code": "CPV",
        "name": "Kap Verde",
    },
    {
        "code": "CUW",
        "name": "Curaçao",
    },
    {
        "code": "HAI",
        "name": "Haiti",
    },
    {
        "code": "GUI",
        "name": "Guinea",
    },
    {
        "code": "TOG",
        "name": "Togo",
    },
    {
        "code": "DOM",
        "name": "Dominikanische Republik",
    },
    {
        "code": "CHI",
        "name": "Chile",
    },
    {
        "code": "BFA",
        "name": "Burkina Faso",
    },
    {
        "code": "GAM",
        "name": "Gambia",
    }
]