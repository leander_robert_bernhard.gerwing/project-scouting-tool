import "@/styles/index.scss"; // imports the default styles
import {dsv} from "d3-fetch";
import BubbleChart, {BubbleChartConfigParam} from "@/charts/bubbleChart.ts";
import Search from "@/search.ts";
import {countries} from "@/countries.ts";
import {numericColumns, Player} from "@/player.ts";
import {getPositionName} from "@/positions.ts";
import RadarChart from "@/charts/radarChart.ts";
import {schemeTableau10, color} from "d3";

const radarChartWrapper = document.querySelector('#radar-chart-wrapper') as HTMLDivElement;
const bubbleChartWrapper = document.querySelector('#bubble-chart-wrapper') as HTMLDivElement;
const zoomExtent: [number, number] = [1, 5]
let bubbleChart: BubbleChart | null = null
let radarChart: RadarChart | null = null
let parsedData: Player[] = []

let sliderBlocked = false
const zoomSlider = document.querySelector('#zoom-slider') as HTMLInputElement
zoomSlider.min = zoomSlider.value = zoomExtent[0].toString()
zoomSlider.max = zoomExtent[1].toString()
zoomSlider.step = '0.1'
zoomSlider.oninput = (event) => {
    if (bubbleChart) {
        const newZoom = parseFloat((event.target as HTMLInputElement).value)
        bubbleChart.zoomTo(newZoom)
    }
}

const groupBySetting = document.querySelector('#group-by-setting') as HTMLInputElement
groupBySetting.oninput = (event) => {
    if (bubbleChart) {
        bubbleChart.updateVisConfig({groupAccessor: (d: any) => d[(event.target as HTMLInputElement).value]} as BubbleChartConfigParam)
    }
}

const colorBySetting = document.querySelector('#color-by-setting') as HTMLInputElement
for (const column of Object.keys(numericColumns)) {
    const option = document.createElement('option')
    option.value = column
    option.text = numericColumns[column as keyof typeof numericColumns]
    colorBySetting.appendChild(option)
}
colorBySetting.oninput = (event) => {
    if (bubbleChart) {
        bubbleChart.updateVisConfig({colorAccessor: (d: any) => d[(event.target as HTMLInputElement).value]} as BubbleChartConfigParam)
    }
}

const sizeBySetting = document.querySelector('#size-by-setting') as HTMLInputElement
for (const column of Object.keys(numericColumns)) {
    const option = document.createElement('option')
    option.value = column
    option.text = numericColumns[column as keyof typeof numericColumns]
    sizeBySetting.appendChild(option)
}
sizeBySetting.oninput = (event) => {
    if (bubbleChart) {
        bubbleChart.updateVisConfig({sizeAccessor: (d: any) => d[(event.target as HTMLInputElement)   .value] ?? 0} as BubbleChartConfigParam)
    }
}
const selectedNodes: (Player & { _color: string})[] = []

const radarChartSettings = document.querySelector('.radar-chart-settings') as HTMLDivElement
const radarChartAttributes = [
    {
        key: 'Performance _ G+A',
        label: numericColumns['Performance _ G+A']
    },
    {
        key: 'Expected_0 _ npxG+xAG',
        label: numericColumns['Expected_0 _ npxG+xAG']
    },
    {
        key: 'PrgP',
        label: numericColumns['PrgP']
    },
    {
        key: 'Total _ Cmp%',
        label: numericColumns['Total _ Cmp%']
    },
    {
        key: 'Tkl+Int',
        label: numericColumns['Tkl+Int']
    },
    {
        key: 'Touches _ Touches',
        label: numericColumns['Touches _ Touches']
    },
]

dsv(';', 'data/output.csv').then(data => {
    parsedData = data.map((d: any) => {
            for (const column of Object.keys(numericColumns)) {
                d[column] = parseFloat(d[column])
            }
            if (countries.findIndex(c => c.code === d.nation) === -1) {
                console.warn('Unknown country code:', d.nation)
            }
            return {
                ...d,
                pos: getPositionName(d.pos),
                nation: countries.find(c => c.code === d.nation)?.name ?? d.nation,
            } as Player
        }
    )

    bubbleChart = new BubbleChart(parsedData, {
        parentElement: bubbleChartWrapper,
        containerWidth: 1000,
        containerHeight: 1000,
        margin: {top: 20, right: 20, bottom: 20, left: 20},
        zoomExtent,
        idAccessor: (d: any) => d.player,
        onZoom: (event) => {
            if (sliderBlocked) return
            zoomSlider.value = event.transform.k.toString()
        },
        renderTooltip: (dataPoint: any, tooltip: d3.Selection<d3.BaseType, unknown, HTMLElement, any>) => {
            const age = dataPoint['age'].split('-')
            tooltip.html(`
                <table>
                    <tr><th>Name</th><td>${dataPoint['player']}</td></tr>
                    <tr><th>Liga</th><td>${dataPoint['league']}</td></tr>
                    <tr><th>Position</th><td>${dataPoint['pos']}</td></tr>
                    <tr><th>Nationalität</th><td>${dataPoint['nation']}</td></tr>
                    <tr><th>Team</th><td>${dataPoint['team']}</td></tr>
                    <tr><th>Alter</th><td>${age[0]}${age[1] ? ' - ' + age[1] + ' Tage' : ''}</td></tr>
                </table>
        `)
        },
        onClick: (dataPoint: any) => {
            updateSelectedNodes(dataPoint)
        },
    })

    bubbleChart.renderVis()
    const search = new Search(bubbleChart)

    search.initOptions(data)
})

const zoomInButton = document.querySelector('#zoom-in') as HTMLButtonElement
zoomInButton.onclick = () => updateZoomLevel('+')

const zoomOutButton = document.querySelector('#zoom-out') as HTMLButtonElement
zoomOutButton.onclick = () => updateZoomLevel('-')

function updateZoomLevel(to: '+' | '-') {
    if (bubbleChart) {
        to === '+' ? bubbleChart.zoomIn() : bubbleChart.zoomOut()
    }
}

function updateSelectedNodes(node: Player) {
    if (selectedNodes.length > 6) {
        alert('Maximal 6 Spieler können ausgewählt werden')
    }
    if (selectedNodes.some(n => n.player === node.player)) {
        selectedNodes.splice(selectedNodes.findIndex((n: Player) => n.player === node.player), 1)
    } else {
        const missingColor = schemeTableau10.find((color) => selectedNodes.findIndex(n => n._color === color) === -1)
        selectedNodes.push({
            ...node,
            _color: missingColor ?? schemeTableau10[selectedNodes.length],
        })
    }
    bubbleChart?.updateSelectedNodes(selectedNodes)
    if (!radarChart) {
        radarChart = new RadarChart(parsedData, {
            parentElement: radarChartWrapper,
            selectedData: selectedNodes,
            containerWidth: 750,
            containerHeight: 750,
            margin: {top: 20, right: 20, bottom: 20, left: 20},
            axisCircles: 2,
            idAccessor: (d: any) => d.player,
            renderTooltip: (dataPoint: any, tooltip: d3.Selection<d3.BaseType, unknown, HTMLElement, any>) => {
                bubbleChart?.zoomToPoint(dataPoint.data.player)
                tooltip.html(`
                <h4>${dataPoint.data['player']}</h4>
                <table>
                    <tr><th>${dataPoint['label']}</th><td>${dataPoint['value']}</td></tr>
                </table>
            `)
            },
            attributes: radarChartAttributes,
        })
        radarChart.renderVis()

        // add attribute settings
        for (const index in radarChartAttributes) {
            const input = document.createElement('select')

            for (const column of Object.keys(numericColumns)) {
                const option = document.createElement('option')
                option.value = column
                option.text = numericColumns[column as keyof typeof numericColumns]
                input.appendChild(option)
            }
            input.value = radarChartAttributes[index].key
            input.oninput = (event) => {
                const value = (event.target as any)?.value
                radarChartAttributes[index] = {
                    key: value,
                    label: numericColumns[value as keyof typeof numericColumns]
                }
                radarChart?.updateSelectedAttribute(radarChartAttributes)
            }

            radarChartSettings.appendChild(input)
        }
    } else {
        radarChart.updateVis(selectedNodes)
    }
    updatePlayerCards()
}

function updatePlayerCards() {
    const playerCardContainer = document.querySelector('#player-cards')
    if (!playerCardContainer) return
    playerCardContainer.innerHTML = ''
    selectedNodes.forEach((node) => {
        const d3color = color(node._color)?.brighter(0.8)
        const card = document.createElement('div')
        card.classList.add('player-card')
        card.style.background = `linear-gradient(135deg, ${node._color}, ${d3color})`
        card.innerHTML = `
            <h3>${node.player}</h3>
            <p>${node.team} - ${node.pos} - ${node.nation} - ${node.age}</p>
        `
        for (const attribute of radarChartAttributes) {
            const value = node[attribute.key as keyof Player]
            card.innerHTML += `
                    <p>${attribute.label}: ${value}</p>
            `
        }
        playerCardContainer.appendChild(card)
    })
}