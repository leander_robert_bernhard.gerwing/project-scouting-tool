# Scouting tool
A visualization of data scraped from FBref.com, containing detailed statistics of soccer players.
Project writeup can be found [here](project-writeup.md).

## Review
A built version of this project is available under the `dist` folder.
The contained index.html file can be opened in a browser, however, you will receive a CORS error when trying to fetch the built assets.
To avoid this, you can run the command `npx vite preview`.

## Getting Started
1. Install dependencies by either running `npm ci` or `yarn`
2. Start the development server by running `npm run dev` or `yarn dev`

You are now ready to start building your D3.js project with TypeScript!

## Building for Production
To build the project for production, run `npm run build` or `yarn build`. The output will be in the `dist` folder.
